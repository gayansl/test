package transforms

import (
	"bitbucket.org/mybudget-dev/stream-connect-worker/connector"
	"encoding/json"
	"fmt"
	"github.com/pickme-go/log"
	"strings"
)

type HoistField struct {
	Type  string
	Field string
}

var hoistFieldLogPrefix = "HoistField SMT"

func (h HoistField) Transform(rec connector.Recode) connector.Recode {
	if strings.Contains(h.Type, "Key") {
		key := h.getJSON(rec.Key())
		if key == nil {
			return NewRec(rec.Key(), rec.Value(), rec.Topic(), rec.Partition(), rec.Offset(), rec.Timestamp())
		}
		return NewRec(key, rec.Value(), rec.Topic(), rec.Partition(), rec.Offset(), rec.Timestamp())
	} else if strings.Contains(h.Type, "Value") {
		value := h.getJSON(rec.Value())
		if value == nil {
			return NewRec(rec.Key(), rec.Value(), rec.Topic(), rec.Partition(), rec.Offset(), rec.Timestamp())
		}
		return NewRec(rec.Key(), value, rec.Topic(), rec.Partition(), rec.Offset(), rec.Timestamp())
	}

	log.Error(log.WithPrefix(hoistFieldLogPrefix, fmt.Sprintf("unknown SMT type must be (HoistField$Key, HoistField$Value): %v", h.Type)))
	return NewRec(rec.Key(), rec.Value(), rec.Topic(), rec.Partition(), rec.Offset(), rec.Timestamp())
}

func (h HoistField) getJSON(value interface{}) interface{} {
	m := make(map[string]interface{})
	m[h.Field] = value
	b, err := json.Marshal(m)
	if err != nil {
		log.Error(log.WithPrefix(hoistFieldLogPrefix, fmt.Sprintf("unknown SMT type must be (HoistField$Key, HoistField$Value): %v", h.Type)))
		return nil
	}
	return string(b)
}
