package transforms

import (
	"bitbucket.org/mybudget-dev/stream-connect-worker/connector"
	"time"
)

type connectRecord struct {
	key       interface{}
	value     interface{}
	topic     string
	partition int32
	timestamp time.Time
	offset    int64
}

func NewRec(key, value interface{}, topic string, partition int32, offset int64, timestamp time.Time) connector.Recode {
	return &connectRecord{key: key, value: value, topic: topic, partition: partition, offset: offset, timestamp: timestamp}
}

func (cr *connectRecord) Key() interface{} {
	return cr.key
}

func (cr *connectRecord) Value() interface{} {
	return cr.value
}

func (cr *connectRecord) Offset() int64 {
	return cr.offset
}

func (cr *connectRecord) Topic() string {
	return cr.topic
}

func (cr *connectRecord) Partition() int32 {
	return cr.partition
}

func (cr *connectRecord) Timestamp() time.Time {
	return cr.timestamp
}
