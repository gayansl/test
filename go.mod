module bitbucket.org/gayansl/test

go 1.12.7

require (
	bitbucket.org/mybudget-dev/stream-connect-worker v0.0.0-20190801104725-fffabe0afc5d
	github.com/pickme-go/log v1.2.3
	github.com/tidwall/gjson v1.3.2
	github.com/tidwall/sjson v1.0.4
)
