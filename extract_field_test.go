package transforms

import (
	"reflect"
	"testing"
	"time"
)

func TestExtractFieldKey_Transform(t *testing.T) {
	castType := "ExtractField&Key"
	tests := []struct {
		key     string
		extract Transformer
		out     interface{}
	}{
		{ // TEST 1, with complex json transformers
			`{"age": "12.2324", "12.32": "100.34412414213412341234123412342134", "user": {"age": "12.456"}}`,
			&ExtractField{castType, "age"},
			`12.2324`},
		{ // TEST 2, with multiple key transformers
			`{"age": "12.2324", "height": "100.34412414213412341234123412342134", "user": {"age": "12.456"}}`,
			&ExtractField{castType, "height"},
			`100.34412414213412341234123412342134`},
		{ // TEST 3, with multiple key transformers
			`{"age": "12.2324", "height": "100.34412414213412341234123412342134", "user": {"age": "12.456"}}`,
			&ExtractField{castType, "user.age"},
			`12.456`},
		{ // TEST 3, with multiple key transformers
			`{"eventDetails":{"eventId":"893fa0a3-5f1c-4f80-a53c-624e2bd4dbcd","referenceId":"2019-08-01-07-51-29.938.0002_default_default","createdTime":1564645890005,"validPeriod":0,"type":"CREATED_ACTIVITY_LOG_EVENT","userDetails":{"userId":"anonymous","userCorrelationId":"anonymous"},"version":"0.0.0"},"payload":{"id":"24e73c6c-6c9c-44ce-9539-c513c29070b6","accountId":"6c6d7cc5-9187-4ac6-bf5b-65633fc47836","activityLogType":{"code":"MILESTONE","description":"Milestone"},"activitySubject":"Hello1111 test John - Complete New Enquiry","milestone":"LEAD_CREATED","detail":null,"created":"2019-08-01T07:51:29.975+0000","assignee":{"id":"anonymous","firstName":"Elizabeth","lastName":"Pelvadore"}}}`,
			&ExtractField{castType, "payload"},
			`{"id":"24e73c6c-6c9c-44ce-9539-c513c29070b6","accountId":"6c6d7cc5-9187-4ac6-bf5b-65633fc47836","activityLogType":{"code":"MILESTONE","description":"Milestone"},"activitySubject":"Hello1111 test John - Complete New Enquiry","milestone":"LEAD_CREATED","detail":null,"created":"2019-08-01T07:51:29.975+0000","assignee":{"id":"anonymous","firstName":"Elizabeth","lastName":"Pelvadore"}}`},
	}

	rec := NewRec(nil, nil, "", 0, 0, time.Now())
	for _, test := range tests {
		// skip test cases
		//if id != 1 {
		//	continue
		//}
		rec = NewRec(test.key, `{}`, "", 0, 0, time.Now())
		rec = test.extract.Transform(rec)

		if test.out != rec.Key() {
			t.Errorf("expected type: %v : %v, but got type: %v : %v ", reflect.TypeOf(test.out), test.out, reflect.TypeOf(rec.Key()), rec.Key())
		}
	}
}

func TestExtractFieldValue_Transform(t *testing.T) {
	castType := "ExtractField&Value"
	tests := []struct {
		value   string
		extract Transformer
		out     interface{}
	}{
		{ // TEST 1, with complex json transformers
			`{"age": "12.2324", "12.32": "100.34412414213412341234123412342134", "user": {"age": "12.456"}}`,
			&ExtractField{castType, "age"},
			`12.2324`},
		{ // TEST 2, with multiple key transformers
			`{"age": "12.2324", "height": "100.34412414213412341234123412342134", "user": {"age": "12.456"}}`,
			&ExtractField{castType, "height"},
			`100.34412414213412341234123412342134`},
		{ // TEST 3, with multiple key transformers
			`{"age": "12.2324", "height": "100.34412414213412341234123412342134", "user": {"age": "12.456"}}`,
			&ExtractField{castType, "user.age"},
			`12.456`},
	}

	rec := NewRec(nil, nil, "", 0, 0, time.Now())
	for _, test := range tests {
		// skip test cases
		//if id != 1 {
		//	continue
		//}
		rec = NewRec(nil, test.value, "", 0, 0, time.Now())
		rec = test.extract.Transform(rec)

		if test.out != rec.Value() {
			t.Errorf("expected type: %v : %v, but got type: %v : %v ", reflect.TypeOf(test.out), test.out, reflect.TypeOf(rec.Value()), rec.Value())
		}
	}
}
